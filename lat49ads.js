
Drupal.gmap.addHandler('gmap', function(elem) {
  var obj = this;
  // Respond to incoming movements.
  // Make it a gmap.
  // Hide away a reference to the map
  //obj.map = map;
  obj.bind("init",function() {
    var map = obj.map;
    GEvent.addListener(obj.map, "click", function(overlay, latlng) {
      var lat = obj.vars.latitude;
      var lng = obj.vars.longitude;
      try {
        var zoom = Lat49.Tile.convertGMap2Zoom(obj.vars.zoom);
        lat49adsGetAds(lat,lng,zoom);
      } catch(e) {}
      });
    GEvent.addListener(obj.map, "moveend", function(overlay, latlng) {
      var lat = obj.vars.latitude;
      var lng = obj.vars.longitude;
      try {
        var zoom = Lat49.Tile.convertGMap2Zoom(obj.vars.zoom);
        lat49adsGetAds(lat,lng,zoom);
      } catch(e) {}
      
      });
  });
});

var AdPushpin = function() {
  //private members
  var map = null;
  var markersArray = new Array();
  //public members
  return {
    // Set the map variable and define the pushpin icon.
    setMap:function(m)
    {
      map = m;
    },
    //Update the marker info with a title and address
    updateMarker:function(marker,title, address)
    {
      GEvent.addListener(marker, "click", function() {
      marker.openInfoWindowHtml("<b>" + title + "</b><br> " + address);});
    },
    //Hide all the pushpins
    hidePushPin:function()
    {
      for (var i = 0; i < markersArray.length; i++)
      {
        map.removeOverlay(markersArray[i]);
      }
      markersArray = new Array();
    },
    //Push new markers into the markersArray and display them on the map.
    //The parameter is an array of objects that include latitude, longitude, title, address, and pinurl.
    //var pin-lat = data[i].lat.
    //var pin-lon = data[i].lon.
    //var pin-title = data[i].title.
    //var pin-address = data[i].address.
    //var pin-url = data[i].pinurl.
    showPushPin:function(loc)
    {
      var maxpins = 7;
      for (var i=0; i<loc.length;i++)
      {
        if(i<=maxpins)
        {
          var point = new GLatLng(loc[i].lat, loc[i].lon);
          var adicon = new GIcon(G_DEFAULT_ICON);
          adicon.image = loc[i].pinurl;
          adicon.shadow ="";
          adicon.iconSize = new GSize(36,32);
          var marker = new GMarker(point,{clickable: true, bouncy: true, icon:adicon});
          map.addOverlay(marker);
          // add markers to array
          markersArray.push(marker);
          this.updateMarker(marker, loc[i].title, loc[i].address);
        }
      }
    } 
  }
}();

function lat49adsGetAds(lat,lng,zoom) {
  //Create an object to map the DIV ids to the desired ad size.
  var ad_types = new Object();
  
  //Need to do checks before actually loading up each placement.
  var content = $("#lat49ads_half_banner");
  if (content.length) {ad_types['lat49ads_half_banner']=Lat49.Ads.HALF_BANNER;}
  
  content = $("#lat49ads_button");
  if (content.length) {ad_types['lat49ads_button']=Lat49.Ads.BUTTON;}
  
  content = $("#lat49ads_leaderboard");
  if (content.length) {ad_types['lat49ads_leaderboard']=Lat49.Ads.LEADERBOARD; }
  
  content = $("#lat49ads_full_banner");
  if (content.length) {ad_types['lat49ads_full_banner']=Lat49.Ads.FULL_BANNER; }
  
  content = $("#lat49ads_medium_rectangle");
  if (content.length) {ad_types['lat49ads_medium_rectangle']=Lat49.Ads.MEDIUM_RECTANGLE; }
  
  Lat49.updateMultiAdsByLatLon(ad_types,lat,lng,zoom);
  
  content = $("#lat49ads_adcontainer");
  if (content.length) { Lat49.updateAdByLatLon('lat49ads_adcontainer',lat,lng,zoom); }
  
}