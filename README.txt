
********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Lat49 Ads module 
Author: Greg Holsclaw
Sponsored by: www.dogparkusa.com
Drupal: 5.x
********************************************************************
DESCRIPTION:

A module which displays map based ads on your Google maps.
Lat49 is the mapvertising system that brings geographically relevant advertising to users of online maps.


********************************************************************
INSTALLATION:

	Note: It is assumed that you have Drupal up and running.  Be sure to
    check the Drupal web site if you need assistance.  If you run into
    problems, you should always read the INSTALL.txt that comes with the
    Drupal package and read the online documentation.

	1. Place the entire lat49ads directory into your Drupal
        modules/directory.

	2. Enable the lat49ads module by navigating to:

	   Administer > Site building > Modules

	Click the 'Save configuration' button at the bottom to commit your
    changes.
    


********************************************************************
CONFIGURATION

	* Make sure you already have the GMaps module installed, configured and rendering maps on your site.
	* Enable the module on the admin/build/modules page.
	* Configure the basic lat49ads module settings at admin/settings/lat49ads.
	* If you also wish to use the ad blocks, configure them at admin/build/block.



********************************************************************

Known Issues


* Issues with 100% width maps.
* More of a Lat49 issue, but only the small 234px x 60px and 125px x 125px size block ads are consistently used as of 10/17/2008.


********************************************************************

Roadmap

* Implement cycling ads options through Lat49
* All for alternative div ID to place ads into (in case of custom theming).
* Integrate with Yahoo Maps (possibly through Node Map module)

